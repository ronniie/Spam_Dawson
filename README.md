![intro](https://chaottiic.com/static/images/logo.png)

# Le Count
#### *Spamming Bot used to Spam @Dawson!*
[<img src="https://discordapp.com/api/guilds/380727258622459904/widget.png?style=banner3">](https://chaottiic.com/discord)


Installation:

> Windows
> - Step #1 | Install Python (3.6.5 Recommended)
> - Step #2 | Install VirtualENV (If not installed)
> - Step #3 | Download the files to a Folder on your Desktop
> - Step #4 | Go to Command Prompt, and go to the folder on your desktop
> - Step #5 | In CMD Prompt. Type `virtualenv env`
> - Step #6 | Then type `.\env\Scripts\activate`
> - Step #7 | Then type `pip install discord.py`
> - Step #8 | Then run `python main.js `

> MacOS
> - Step #1 | Install Python (3.6.5 Recommended)
> - Step #2 | Install VirtualENV (If not installed; https://sourabhbajaj.com/mac-setup/Python/virtualenv.html)
> - Step #3 | Download the files to a Folder on your Desktop
> - Step #4 | Go to Command Prompt, and go to the folder on your desktop
> - Step #5 | In CMD Prompt. Type `virtualenv env`
> - Step #6 | Then type `source env/bin/activate`
> - Step #7 | Then type `pip install discord.py`
> - Step #8 | Then run `python main.js `